using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class chestScript : MonoBehaviour
{

    public List<ItemContainner> item = new List<ItemContainner>();

    public ItemStore store;

    public List<int> elementToLoad = new List<int>();

    public GameObject inventryItems;

    private int length;

    private int randomNumber = 0;

    [HideInInspector]
    public bool load = false;
    [HideInInspector]
    public bool unload = false;
    [HideInInspector]
    public int loadState = 1;

    [SerializeField]
    private int maxElementsInStore;
    private int minStorageLength = 4;
    private int MaxStorageLength = 8;
    // Start is called before the first frame update
    void Start()
    {
        //this.GetComponent<chestScript>().maxElementsInStore = item.Count;
        load = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (!this.GetComponent<chestScript>().unload)
        {
            for (int i = 0; i < length; i++)
            {
                if (inventryItems.transform.GetChild(i).GetComponent<slot>().type == null)
                {
                    item[i] = null;
                }
                else
                {
                    item[i].itemName = inventryItems.transform.GetChild(i).GetComponent<slot>().itemName;
                    item[i].id = inventryItems.transform.GetChild(i).GetComponent<slot>().id;
                    item[i].type = inventryItems.transform.GetChild(i).GetComponent<slot>().type;
                    item[i].qty = inventryItems.transform.GetChild(i).GetComponent<slot>().qty;
                    item[i].description= inventryItems.transform.GetChild(i).GetComponent<slot>().description;
                    item[i].icon = inventryItems.transform.GetChild(i).GetComponent<slot>().icon;
                }
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == Tags.PlayerTag)
        {
            if (loadState == 1)
            {
                RandomOptionIndexGenerator(1, maxElementsInStore);
                if (this.GetComponent<chestScript>().load)
                {
                    for (int i = 0; i < length; i++)
                    {
                        item[i] = store.items[elementToLoad[i]];
                        dataAssigner(i, store.items[elementToLoad[i]].id, store.items[elementToLoad[i]].type, store.items[elementToLoad[i]].itemName, store.items[elementToLoad[i]].qty, store.items[elementToLoad[i]].description, store.items[elementToLoad[i]].icon, new Color32(255, 255, 255, 255), true);
                    }
                    load = false;
                }
                loadState += 1;
            }
            if (loadState == 2)
            {
                for (int i = 0; i < length; i++)
                {
                    //print("Enter");
                    if (item[i] != null)
                    {
                        dataAssigner(i, item[i].id, item[i].type, item[i].itemName, item[i].qty, item[i].description, item[i].icon, new Color32(255, 255, 255, 255), true);
                    }
                }
            }

            unload = false;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == Tags.PlayerTag)
        {
            //unload 
            unload = true;
            for (int i = 0; i < length; i++)
            {
                dataAssigner(i, 0, null, null, 0, null, null, new Color32(0, 0, 0, 0), false);
            }
        }

    }

    private void dataAssigner(int i, int ID, string TYPE, string ITEMNAME, int QTY, string DISCRIPTION, Sprite ICON, Color32 color, bool value)
    {

        if (item[i] != null)
        {
            inventryItems.transform.GetChild(i).GetChild(0).GetComponent<Image>().color = color;
            inventryItems.transform.GetChild(i).GetChild(0).GetComponent<drag>().enabled = value;
        }
        inventryItems.transform.GetChild(i).GetComponent<slot>().id = ID;
        inventryItems.transform.GetChild(i).GetComponent<slot>().type = TYPE;
        inventryItems.transform.GetChild(i).GetComponent<slot>().itemName = ITEMNAME;
        inventryItems.transform.GetChild(i).GetComponent<slot>().qty = QTY;
        inventryItems.transform.GetChild(i).GetComponent<slot>().description = DISCRIPTION;
        inventryItems.transform.GetChild(i).GetComponent<slot>().icon = ICON;
        inventryItems.transform.GetChild(i).GetChild(0).GetComponent<Image>().sprite = ICON;
    }

    //random option index
    public void RandomOptionIndexGenerator(int initial, int last)
    {
        length = Random.Range(minStorageLength, MaxStorageLength);
        elementToLoad = new List<int>(new int[length]);
        for (int i = 0; i < length; i++)
        {
            randomNumber = Random.Range(initial, last);
            while (elementToLoad.Contains(randomNumber))
            {
                randomNumber = Random.Range(initial, last);
            }
            elementToLoad[i] = randomNumber;
        }
        for (int i = 0; i < length; i++)
        {
            elementToLoad[i] -= 1;
        }
    }

}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryBox : MonoBehaviour
{

    public GameObject loadingVisuals;
    public Image loadingBar;

    public GameObject playerPreviews;
    public GameObject inventryStroagePreview;

    public static bool enableInventry;
    private bool load = false;
    [HideInInspector]
    public bool isOpened = false;
    private bool allowedQ;
    private bool isQPressed;

    public GameObject buttonQ;

    private float fillSpeed = 30;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.I))
        {
            //print("pressed");
            playerPreviews.SetActive(true);
            inventryStroagePreview.SetActive(false);
        }
        if (allowedQ)
        {
            if (Input.GetKeyDown(KeyCode.Q))
            {
                //print("load");
                this.GetComponent<InventoryBox>().isQPressed = true;
                buttonQ.SetActive(false);
                if (!this.GetComponent<InventoryBox>().isOpened)
                {
                    this.GetComponent<InventoryBox>().load = true;
                    loadingVisuals.SetActive(true);
                }
                else
                {
                    enableInventry = true;
                }
                this.GetComponent<InventoryBox>().allowedQ = false;
                playerPreviews.SetActive(false);
                inventryStroagePreview.SetActive(true);
            }
        }
        if (this.GetComponent<InventoryBox>().load)
        {
            loadingBar.fillAmount += fillSpeed * Time.deltaTime * Time.deltaTime;
            if (loadingBar.fillAmount == 1)
            {
                //print("direct");
                enableInventry = true;
                this.GetComponent<InventoryBox>().load = false;
                this.GetComponent<InventoryBox>().isOpened = true;
                loadingVisuals.SetActive(false);
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == Tags.PlayerTag)
        {
            if (!this.GetComponent<InventoryBox>().isQPressed)
            {
                buttonQ.SetActive(true);
                this.GetComponent<InventoryBox>().allowedQ = true;
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == Tags.PlayerTag)
        {
            loadingBar.fillAmount = 0;
            this.GetComponent<InventoryBox>().isQPressed = false;
            buttonQ.SetActive(false);
        }
    }

}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeaponSwipe : MonoBehaviour
{

    public GameObject weaponOne;
    public GameObject weaponTwo;

    public static int currentActiveWeapon;

    public GameObject tempObject;
    // Start is called before the first frame update
    void Start()
    {
        Instantiate(tempObject);
    }

    // Update is called once per frame
    void Update()
    {
        currentActiveWeapon = weaponOne.GetComponent<slot>().id;
        if (currentActiveWeapon == 0)
        {
            WeaponManager.weaponLoaded = false;
        }
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            swipGameObjects(weaponOne, weaponTwo, tempObject);
            weaponOne.transform.GetChild(0).transform.GetComponent<Image>().sprite = weaponOne.GetComponent<slot>().icon;
            weaponTwo.transform.GetChild(0).transform.GetComponent<Image>().sprite = weaponTwo.GetComponent<slot>().icon;
        }
    }

    private void swipGameObjects(GameObject obj1, GameObject obj2, GameObject tempObj)
    {
        storeIn(obj1, tempObj);
        storeIn(obj2, obj1);
        storeIn(tempObj, obj2);
    }

    private void storeIn(GameObject obj1, GameObject obj2)
    {
        //print(obj2.GetComponent<slot>().id);
        obj2.GetComponent<slot>().id = obj1.GetComponent<slot>().id;
        obj2.GetComponent<slot>().type = obj1.GetComponent<slot>().type;
        obj2.GetComponent<slot>().itemName = obj1.GetComponent<slot>().itemName;
        obj2.GetComponent<slot>().qty = obj1.GetComponent<slot>().qty;
        obj2.GetComponent<slot>().description = obj1.GetComponent<slot>().description;
        obj2.GetComponent<slot>().icon = obj1.GetComponent<slot>().icon;
    }

}

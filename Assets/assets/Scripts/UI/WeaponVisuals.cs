using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeaponVisuals : MonoBehaviour
{
    [SerializeField]
    private GameObject weaponOneVisuals;
    [SerializeField]
    private GameObject weaponTwoVisuals;

    [SerializeField]
    private GameObject weaponOneSlot;
    [SerializeField]
    private GameObject weaponTwoSlot;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!weaponOneSlot.transform.parent.transform.GetComponent<slot>().empty)
        {
            weaponOneVisuals.GetComponent<Image>().sprite = weaponOneSlot.GetComponent<Image>().sprite;
            weaponOneVisuals.GetComponent<Image>().color = new Color32(255, 255, 255, 255);
        }
        if (!weaponTwoSlot.transform.parent.transform.GetComponent<slot>().empty)
        {
            weaponTwoVisuals.GetComponent<Image>().sprite = weaponTwoSlot.GetComponent<Image>().sprite;
            weaponTwoVisuals.GetComponent<Image>().color = new Color32(255, 255, 255, 255);
        }
    }
}
